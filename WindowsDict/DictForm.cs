﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using MySql.Data;
using MySql.Data.MySqlClient;
using Lucene.Net.Analysis;
using System.IO;
using System.Text.RegularExpressions;

namespace WindowsDict
{
    public partial class DictForm : Form
    {

        string strConnectionString = string.Empty,/*SQLite连接字符串，刚开始没有，暂时留空*/
                strDataSource = "dic.dll";//SQLite数据库文件存放物理地址
        //用SQLiteConnectionStringBuilder构建SQLite连接字符串
        SQLiteConnectionStringBuilder scBuilder = new SQLiteConnectionStringBuilder();

        public DictForm()
        {
            InitializeComponent();
            scBuilder.DataSource = strDataSource;//SQLite数据库地址
            scBuilder.Password = "www.52en.me";//密码
            strConnectionString = scBuilder.ToString();
        }     

        private void button4_Click(object sender, EventArgs e)
        {
            translationButton.Enabled = false;
            string s = leftTextBox.Text;
            Analyzer analyzer = new StopAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
            HashSet<string> words = new HashSet<string>();
            System.IO.StringReader reader = new System.IO.StringReader(s);
            TokenStream ts = analyzer.ReusableTokenStream("", reader);
            Lucene.Net.Analysis.Tokenattributes.ITermAttribute ita;
            bool hasnext = ts.IncrementToken();

            while (hasnext)
            {
                ita = ts.GetAttribute<Lucene.Net.Analysis.Tokenattributes.ITermAttribute>();
                Console.WriteLine(ita.Term);
                words.Add(ita.Term);
                hasnext = ts.IncrementToken();
            }
            ts.CloneAttributes();
            reader.Close();
            analyzer.Close();
            List<string> tags = new List<string>();
            if (zk.Checked) {
                tags.Add(zk.Tag.ToString());
            }
            if (gk.Checked)
            {
                tags.Add(gk.Tag.ToString());
            }
            if (cet4.Checked)
            {
                tags.Add(cet4.Tag.ToString());
            }
            if (cet6.Checked)
            {
                tags.Add(cet6.Tag.ToString());
            }
            if (ky.Checked)
            {
                tags.Add(ky.Tag.ToString());
            }
            if (toefl.Checked)
            {
                tags.Add(toefl.Tag.ToString());
            }
            if (ielts.Checked)
            {
                tags.Add(ielts.Tag.ToString());
            }
            if (gre.Checked)
            {
                tags.Add(gre.Tag.ToString());
            }

            rightTextBox.Text = getWord(words, tags, zhtrans.Checked, entrans.Checked).Replace("\\n", "\r\n");
            translationButton.Enabled = true;

        }

        private string getWord(HashSet<string> words, List<string> tags, bool zhtrans, bool entrans)
        {
            StringBuilder sb = new StringBuilder();

            using (SQLiteConnection connection = new SQLiteConnection(strConnectionString))
            {

                //打开数据连接
                connection.Open();
                //Command
                SQLiteCommand command = new SQLiteCommand(connection);
                command.CommandText = "select word, phonetic,  definition, translation, pos,collins, oxford, tag, bnc, frq, exchange from words where word = @word";
                command.CommandType = System.Data.CommandType.Text;
                long i = 1;
                foreach (string w in words)
                {
                    command.Parameters.AddWithValue("@word", w);
                    SQLiteDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {               
                        while (reader.Read())
                        {
                            bool show = false;
                            string tag = reader.GetString(7);
                            if (tags.Count == 0)
                            {
                                show = true;
                            }
                            else
                            {
                                foreach (string t in tags)
                                {
                                    if (tag.Contains(t))
                                    {
                                        show = true;
                                        break;
                                    }
                                }
                            }

                            if (show)
                            {
                                sb.AppendLine();
                                sb.Append(i++).Append(">");
                                sb.Append(w).Append(" [").Append(reader.GetString(1)).Append("]").Append(" ").Append(tag).AppendLine();
                                if (zhtrans)
                                {
                                    sb.Append(reader.GetString(3).Trim('"')).AppendLine();
                                }
                                if (entrans)
                                {
                                    sb.Append(reader.GetString(2).Trim('"')).AppendLine();
                                }
                            }
                           
                        }
                    }
                    else
                    {
                        sb.Append(i++).Append(">").Append(w).Append(" 未找到翻译").AppendLine();
                    }               
                    reader.Close();
                }

                //=======关闭连接
                connection.Close();
            }

            return sb.ToString();
        }     
    }
}
